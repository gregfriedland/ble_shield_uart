#define BUFFER_LENGTH 128

typedef byte BufferType;

bool ble_is_connected(void);

int ble_pop_rx_data(BufferType *buffer, int len, int endChar=-1);
bool ble_tx(BufferType *buffer, int len);

void ble_setup(void (*_connCallback)(void), void (*_disconnCallback)(void));
void ble_loop();
