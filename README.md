BLE shield UART library for Arduino Due
=======================================

This project contains a library for sending data to and from the [RedBearLab BLE shield](http://redbearlab.com/bleshield/). This library, in my hands, holds more stable connections and reacts better to disconnects than the RedBearLab BLE Shield library provided in the [SDK](https://github.com/RedBearLab/Release/tree/master/BLEShield).
The code was adapted from (and I could not have written it without) David Edwin's [ble_uart_template project](http://developer.bluetooth.org/Forum/Pages/Forum.aspx?action=ViewPosts&fid=6&tid=63). It has a few changes from that project:

* Made into library form including a `ble_tx()` method for sending data
* Enabled callbacks on connect and disconnect
* Added example .ino project `Loopback.ino` which echos received data back to the sender

It works with the [nRF UART](http://www.nordicsemi.com/eng/Products/Bluetooth-R-low-energy/nRF-UART-App) iOS app distributed by Nordic Semi which has source code [available](http://developer.bluetooth.org/Forum/Pages/Forum.aspx?action=ViewPosts&fid=6&tid=63).

**NOTE:** This library will currently only work with the **Due**, however, there is also a version of the template which runs on Arduino Uno and Mega at the link above. If there is sufficient interest, I could take a stab at making the library compatible with the Uno, Mega, etc.

Requirements:
-------------
* [David Edwin's BLE library for Arduino Due](http://developer.bluetooth.org/Forum/Pages/Forum.aspx?action=ViewPosts&fid=6&tid=63) (I used [v0.5 RC3](http://developer.bluetooth.org/Forum/Pages/Forum.aspx?action=ViewAttachment&paid=14))

Example sketch
-------------
    // This example sketch for the BLE shield UART library does the following:
    //  -Sets up a connection with callbacks for connects and disconnects
    //  -Listens for data
    //  -If data is received, it is echoed back to the sender

    #include <SPI.h>
    #include <ble_system.h>
    #include <ble_shield_uart.h>

    #define RX_BUFFER_SIZE 128
    byte rx_buffer[RX_BUFFER_SIZE];

    void connCallback() {}
    void disconnCallback() {}

    void setup() {
      Serial.begin(57600);
      ble_setup(connCallback, disconnCallback);
    }

    void loop() {
      ble_loop();
      
      int nBytes = ble_pop_rx_data(rx_buffer, RX_BUFFER_SIZE);
      
      if (nBytes > 0) {
        Serial.print("Received ("); Serial.print(nBytes); Serial.print("): [");
        for (int i=0; i<nBytes; i++) {
          Serial.print(rx_buffer[i], DEC);
          Serial.print(" ");
        }
        Serial.println("]");
        
        ble_tx(rx_buffer, nBytes);
      }
    }

API
---
*    `void ble_setup(void (*_connCallback)(void), void (*_disconnCallback)(void))`
     Start listening for BLE connections; call the specified callbacks on connect and disconnect
*    `void ble_loop()`
     Call in loop() as often as possible to process BLE actions, such as receiving data into the receive buffer
*    `bool ble_is_connected(void)`
     Returns whether a BLE connection is active
*    `int ble_pop_rx_data(byte *buffer, int len, int endChar=-1)`
     Pop data from the front of the receive buffer and copy into `buffer`. `buffer`'s length is specified by `len` and the number of bytes copied is returned. If `endChar` >= 0, the popped data will return the bytes until the first instance of `endChar`.
*    `void ble_tx(byte *buffer, int len)`
     Transmit `len` bytes from `buffer`.



Other useful links:
-------------------
* [Discussion of this library on the RedBearLab forum](https://redbearlab.zendesk.com/entries/23440476-Getting-Started)